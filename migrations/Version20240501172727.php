<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240501172727 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE todos ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE todos ADD CONSTRAINT FK_CD826255C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_CD826255C4663E4 ON todos (page_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE todos DROP FOREIGN KEY FK_CD826255C4663E4');
        $this->addSql('DROP INDEX IDX_CD826255C4663E4 ON todos');
        $this->addSql('ALTER TABLE todos DROP page_id');
    }
}
