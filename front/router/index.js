import { createMemoryHistory, createRouter } from 'vue-router'

import Todos from '@/todos.vue'

const routes = [
  { path: 'pages/:id/todos', component: Todos },
]
router.beforeEach(to => {
    if (!hasNecessaryRoute(to)) {
      router.addRoute(generateRoute(to))
      // trigger a redirection
      return to.fullPath
    }
  })
const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

export default router