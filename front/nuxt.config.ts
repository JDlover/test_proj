// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ["vuetify-nuxt-module", "nuxt-lodash"],
  css: ["@/assets/css/main.scss"],
  nitro: {
    // routeRules: {
    //   "/api/": { proxy: "http://localhost:8000" },
    // },
    devProxy: {
      "/api/": {
        target: "http://localhost:8000",
        changeOrigin: true,
      },
    },
  },
});
