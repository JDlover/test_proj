<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;


/**
 * @extends ServiceEntityRepository<Page>
 *
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Page::class);
  }
  public function lastPage()
  {
    $qb = $this->createQueryBuilder('p')
      ->select('MAX(p.number)')
      ->getQuery()
      ->getSingleScalarResult();
    $query = $qb;

    return $query;
  }
  public function prevPage(int $number)
  {
    $qb = $this->createQueryBuilder('p')
      ->where('p.number = :number-1')
      ->setParameter('number', $number);

    $query = $qb->getQuery();

    $array = $query->getOneOrNullResult();
    return $array;
  }
  public function nextPage(int $number)
  {
    $qb = $this->createQueryBuilder('p')
      ->where('p.number = :number+1')
      ->setParameter('number', $number);

    $query = $qb->getQuery();

    $array = $query->getOneOrNullResult();
    return $array;
  }
  public function pagesAfter(int $number)
  {
    $qb = $this->createQueryBuilder('p')
      ->where('p.number > :number')
      ->setParameter('number', $number);

    $query = $qb->getQuery();

    $array = $query->getResult();
    dump($array);
    return $array;
  }
  //    /**
  //     * @return Page[] Returns an array of Page objects
  //     */
  //    public function findByExampleField($value): array
  //    {
  //        return $this->createQueryBuilder('p')
  //            ->andWhere('p.exampleField = :val')
  //            ->setParameter('val', $value)
  //            ->orderBy('p.id', 'ASC')
  //            ->setMaxResults(10)
  //            ->getQuery()
  //            ->getResult()
  //        ;
  //    }

  //    public function findOneBySomeField($value): ?Page
  //    {
  //        return $this->createQueryBuilder('p')
  //            ->andWhere('p.exampleField = :val')
  //            ->setParameter('val', $value)
  //            ->getQuery()
  //            ->getOneOrNullResult()
  //        ;
  //    }
}
