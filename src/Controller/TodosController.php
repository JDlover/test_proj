<?php

namespace App\Controller;

use App\Entity\TodoItem;
use App\Entity\Page;
use App\Repository\PageRepository;
use App\Repository\TodoItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TodosController extends AbstractController
{
  #[Route('/pages/{number}/todos', methods: 'GET')]
  public function allTodos(Page $page, PageRepository $pageRepository, int $number): JsonResponse
  {
    $number = $page->getNumber();
    $completed = $page->getTodoItems()->filter(fn(TodoItem $item) => $item->isCompleted());
    $notCompleted = $page->getTodoItems()->filter(fn(TodoItem $item) => !$item->isCompleted());

    $title = $page->getTitle();
    $lastPage = $pageRepository->lastPage();
    $common = [
      'completed' => $completed->getValues(),
      'not_completed' => $notCompleted->getValues(),
      'page' => $page->getNumber(),
      'title' => $title,
      'last_page' => $lastPage
    ];
    dump($page->getNumber());

    if ($number > 1) {
      $prevPage = $pageRepository->prevPage($number);
      $prevPageCompleted = $prevPage->getTodoItems()->filter(fn(TodoItem $item) => $item->isCompleted());
      $prevPageNotCompleted = $prevPage->getTodoItems()->filter(fn(TodoItem $item) => !$item->isCompleted());
      $additionalNext = [
        'prev_page_completed' => $prevPageCompleted->getValues(),
        'prev_page_not_completed' => $prevPageNotCompleted->getValues(),
      ];
      dump($number);
    }
    if ($number < $lastPage) {
      $nextPage = $pageRepository->nextPage($number);
      $nextPageCompleted = $nextPage->getTodoItems()->filter(fn(TodoItem $item) => $item->isCompleted());
      $nextPageNotCompleted = $nextPage->getTodoItems()->filter(fn(TodoItem $item) => !$item->isCompleted());
      $additionalPrev = [
        'next_page_completed' => $nextPageCompleted->getValues(),
        'next_page_not_completed' => $nextPageNotCompleted->getValues(),
      ];
    }
    $response = array_merge($common, @$additionalPrev ?: [], @$additionalNext ?: []);
    return new JsonResponse($response);
  }
  #[Route('/pages/{number}/todos/add', methods: 'POST')]
  public function createTodo(Request $request, Page $page, EntityManagerInterface $entityManager): Response
  {
    $title = json_decode($request->getContent(), true)['data']['title'];
    $todo = new TodoItem();
    $todo->setTitle($title);
    $todo->setCompleted(false);
    $todo->setPage($page);
    $entityManager->persist($todo);
    $entityManager->flush();
    $todos = $entityManager->getRepository(TodoItem::class)->findAll();
    dump($todo);
    return new JsonResponse($todos);
  }
  #[Route('/pages/add', methods: 'POST')]
  public function createPage(EntityManagerInterface $entityManager): Response
  {
    $page = new Page();
    $page->setTitle("Новая страница");
    $page->setNumber($entityManager->getRepository(Page::class)->lastPage() + 1);
    $entityManager->persist($page);
    $entityManager->flush();
    dump($page);
    return new JsonResponse($page);
  }
  #[Route('/pages/edit/{number}', methods: 'PATCH')]
  public function editPage(Request $request, Page $page, EntityManagerInterface $entityManager, int $number): Response
  {
    $number = $page->getNumber();
    $title = json_decode($request->getContent(), true)['title'];
    $page->setTitle($title);
    $entityManager->flush();
    return new JsonResponse($page);
  }
  #[Route('/pages/delete/{number}')]
  public function deletePage(Page $page, EntityManagerInterface $entityManager, int $number): Response
  {
    $number = $page->getNumber();
    $pagesAfter = $entityManager->getRepository(Page::class)->pagesAfter($number);
    $entityManager->remove($page);
    foreach($pagesAfter as $page) {
      $number = $page->getNumber();
      $page->setNumber($number-1);
    }
    $entityManager->flush();
    $pages = $entityManager->getRepository(Page::class)->findAll();
    return new JsonResponse($pages);
  }
  #[Route('/todos/edit/{id}')]
  public function editTodo(EntityManagerInterface $entityManager, int $id): Response
  {
    $request = Request::createFromGlobals();
    $title = json_decode($request->getContent(), true)['title'];
    $todo = $entityManager->getRepository(TodoItem::class)->find($id);
    // if (!$title) {
    //     throw $this->createNotFoundException(
    //         'Ты не ввел дело, идиот'
    //     );
    // }
    // if ($oldTitle == $title) {
    //     throw $this->createNotFoundException(
    //         'Дебил, это дело нифига не изменилось'
    //     );
    // }
    $todo->setTitle($title);
    $entityManager->flush();
    $todos = $entityManager->getRepository(TodoItem::class)->findAll();
    return new JsonResponse($todos);
  }
  #[Route('/todos/delete/{id}')]
  public function deleteTodo(EntityManagerInterface $entityManager, int $id): Response
  {
    $todo = $entityManager->getRepository(TodoItem::class)->find($id);
    $entityManager->remove($todo);
    $entityManager->flush();
    $todos = $entityManager->getRepository(TodoItem::class)->findAll();
    return new JsonResponse($todos);
  }
  #[Route('/todos/complete/{id}')]
  public function completeTodo(EntityManagerInterface $entityManager, int $id): Response
  {
    $todo = $entityManager->getRepository(TodoItem::class)->find($id);
    $todo->setCompleted(true);
    $entityManager->flush();
    $todos = $entityManager->getRepository(TodoItem::class)->findAll();
    return new JsonResponse($todos);
  }
  #[Route('/todos/{id}')]
  public function show(TodoItemRepository $todosRepository, int $id): JsonResponse
  {
    $todo = $todosRepository
      ->find($id);

    return new JsonResponse($todo);
  }
  // #[Route('/todos/create', name: 'fill_db')]
  // public function fillDb(EntityManagerInterface $entityManager): Response
  // {
  //     $json = file_get_contents('todos.json');
  //     $data = json_decode($json, true);
  //     foreach ($data as $todoData) {
  //         $todo = new Todos();
  //         $todo->setTitle($todoData['title']);
  //         $todo->setCompleted($todoData['completed']);
  //         $entityManager->persist($todo);
  //         $entityManager->flush();
  //     }

  //     return new Response('db filled' . $todo->getId());
  // }
}
